with sales as (
    SELECT
        sales.id,
        sales.order_id,
        sales.customer_id,
        sales.product_id,
        sales.quantity,
        sales.price,
        (sales.quantity*sales.price) as tot_price,
        to_date(sales.order_date, 'YYYY-MM-DD') as order_date,
        sales.customer_name as customer_name,
        sales.customer_username as customer_username,
        sales.customer_email as customer_email,
        cast(sales.customer_lat as float) as customer_lat,
        cast(sales.customer_lng as float) as customer_lng,
        sales.weather_condition as weather_condition,
        sales.weather_temp as weather_temp
    FROM
        {{ source('raw', 'sales') }} sales
)
SELECT
   * 
FROM
    sales