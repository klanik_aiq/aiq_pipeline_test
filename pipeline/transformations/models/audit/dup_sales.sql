WITH dup_order_id as (
    SELECT
        *,
        'order_id' as dup_column
    FROM
        {{ source('raw', 'sales') }}
    WHERE
        order_id in (
            SELECT
                order_id
            FROM
                (
                    SELECT
                        order_id,
                        count(order_id)
                    FROM
                        {{ source('raw', 'sales') }}
                    group by
                        order_id
                    having
                        count(order_id) > 1
                )
        )
)
SELECT
    *
FROM
    dup_order_id