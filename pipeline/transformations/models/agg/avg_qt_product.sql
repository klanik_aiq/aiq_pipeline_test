SELECT
    DISTINCT s.product_id,
    AVG(s.quantity) as avg_order_qt
FROM 
    {{ref('fct_sales')}} s
group by 1