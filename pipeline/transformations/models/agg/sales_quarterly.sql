WITH quarterly_sales AS (
    SELECT
        c.year,
        c.quarter,
        SUM(tot_price) AS total_sales
    FROM
        {{ref('fct_sales')}} s
        JOIN {{ref('dim_calendar')}} c ON (s.order_date=c.calendar_date)
    GROUP BY
        1,2
)
SELECT
    qs.year,
    qs.quarter,
    qs.total_sales
FROM
    quarterly_sales qs