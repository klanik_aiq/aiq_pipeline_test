SELECT
    DISTINCT s.weather_condition,
    AVG(s.tot_price) as avg_sales_amt
FROM 
    {{ref('fct_sales')}} s
group by 1