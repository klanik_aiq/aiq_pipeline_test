with ranked_customers as (
    SELECT
        *,
        ROW_NUMBER() OVER (
            PARTITION BY 1
            ORDER BY
                tot_amnt DESC
        ) AS rank
    FROM
        {{ ref('tot_sales_customer') }}
)
SELECT
    DISTINCT c.customer_id,
    c.customer_name,
    c.customer_username,
    c.customer_email,
    c.customer_lat,
    c.customer_lng,
    c.tot_amnt
FROM
    ranked_customers c
WHERE
    rank <= 5