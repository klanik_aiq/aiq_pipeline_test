SELECT
    DISTINCT c.customer_id,
    c.customer_name,
    c.customer_username,
    c.customer_email,
    c.customer_lat,
    c.customer_lng,
    SUM(s.tot_price) as tot_amnt
FROM 
    {{ref('fct_sales')}} s
    JOIN
    {{ref('dim_customers')}} c ON (s.customer_id=c.customer_id)
group by 1,2,3,4,5,6