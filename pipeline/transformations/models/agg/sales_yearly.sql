WITH yearly_sales AS (
    SELECT
        c.year,
        SUM(tot_price) AS total_sales
    FROM
        {{ref('fct_sales')}} s
        JOIN {{ref('dim_calendar')}} c ON (s.order_date=c.calendar_date)
    GROUP BY
        1
)
SELECT
    ys.year,
    ys.total_sales
FROM
    yearly_sales ys