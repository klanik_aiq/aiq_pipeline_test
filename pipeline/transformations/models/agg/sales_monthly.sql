WITH monthly_sales AS (
    SELECT
        c.year,
        c.month,
        SUM(tot_price) AS total_sales
    FROM
        {{ref('fct_sales')}} s
        JOIN {{ref('dim_calendar')}} c ON (s.order_date=c.calendar_date)
    GROUP BY
        1,2
)
SELECT
    ms.year,
    ms.month,
    ms.total_sales
FROM
    monthly_sales ms