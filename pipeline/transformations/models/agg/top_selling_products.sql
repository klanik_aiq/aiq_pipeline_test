with ranked_products as (
    SELECT
        product_id,
        ROW_NUMBER() OVER (
            PARTITION BY 1
            ORDER BY
                SUM(quantity) DESC
        ) AS rank,
        SUM(quantity) as qnt
    FROM
        {{ ref('fct_sales') }}
    group by product_id
)
SELECT
    DISTINCT p.product_id,
    qnt
FROM
    ranked_products p
WHERE
    rank <= 5