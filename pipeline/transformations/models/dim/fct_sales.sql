with fct_sales as(
    SELECT
        DISTINCT id as sale_id,
        order_id,
        customer_id,
        product_id,
        quantity,
        price,
        tot_price,
        order_date,
        weather_condition,
        weather_temp
    FROM 
        {{ ref('sales') }}
)
SELECT * from fct_sales