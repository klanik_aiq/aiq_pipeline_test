with calendar as (
    SELECT
        CAST(calendar_date AS DATE) as calendar_date,
        EXTRACT(
            YEAR
            FROM
                calendar_date
        ) AS year,
        EXTRACT(
            MONTH
            FROM
                calendar_date
        ) AS month,
        EXTRACT(
            QUARTER
            FROM
                calendar_date
        ) AS quarter
    FROM
        (
            SELECT
                generate_series('2022-01-01', '2024-01-01', interval '1 day') as calendar_date
        )
)
SELECT
    *
FROM
    calendar