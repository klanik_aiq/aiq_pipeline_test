with customers as (
    SELECT
        DISTINCT customer_id,
        customer_name,
        customer_username,
        customer_email,
        customer_lat,
        customer_lng
    FROM
        {{ ref('sales') }}
)
SELECT * FROM customers