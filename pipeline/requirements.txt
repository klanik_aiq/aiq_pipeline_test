agate==1.7.1
annotated-types==0.6.0
attrs==23.2.0
Babel==2.14.0
certifi==2024.2.2
cffi==1.16.0
charset-normalizer==3.3.2
click==8.1.7
colorama==0.4.6
contourpy==1.2.0
cycler==0.12.1
dbt-core==1.7.10
dbt-extractor==0.5.1
dbt-postgres==1.7.10
dbt-semantic-interfaces==0.4.4
fonttools==4.50.0
greenlet==3.0.3
idna==3.6
importlib-metadata==6.11.0
isodate==0.6.1
Jinja2==3.1.3
jsonschema==4.21.1
jsonschema-specifications==2023.12.1
kiwisolver==1.4.5
leather==0.4.0
Logbook==1.5.3
MarkupSafe==2.1.5
mashumaro==3.12
matplotlib==3.8.3
minimal-snowplow-tracker==0.0.2
more-itertools==10.2.0
msgpack==1.0.8
networkx==3.2.1
numpy==1.26.4
packaging==24.0
pandas==2.2.1
parsedatetime==2.6
pathspec==0.11.2
pillow==10.2.0
protobuf==4.25.3
psycopg2-binary==2.9.9
pycparser==2.21
pydantic==2.6.4
pydantic_core==2.16.3
pyparsing==3.1.2
python-dateutil==2.9.0.post0
python-slugify==8.0.4
pytimeparse==1.1.8
pytz==2024.1
PyYAML==6.0.1
referencing==0.34.0
requests==2.31.0
rpds-py==0.18.0
seaborn==0.13.2
six==1.16.0
SQLAlchemy==2.0.29
sqlparse==0.4.4
text-unidecode==1.3
typing_extensions==4.10.0
tzdata==2024.1
urllib3==1.26.18
zipp==3.18.1
