import requests

# A class represents an API client for requesting API's endpoints
class ApiClient:
  def __init__(self, base_url):
    """
      Initialize a new API client object.

      :param base_url: The base URL of the API's endpoints.
    """
    self.base_url = base_url

  def get(self, endpoint, headers, params):
    """
      Makes a get request for an endpoints.

      :param endpoint: The endpoint's path.
      :param headers: The header's data that will be sent with the request.
      :param params: The endpoint's get request parameters.
    """
    url = f"{self.base_url}/{endpoint}"
    
    response = requests.get(url, headers=headers, params=params)
    # Check the response
    if response.status_code == 200:
        data = response.json()
        return data
            
    else:
        print(f"Request {response.request.url} failed with status code {response.status_code}")