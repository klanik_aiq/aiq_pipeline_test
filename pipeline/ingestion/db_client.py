from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import CreateSchema
from models import Base

# A class represents PostgresDB client based on SQLAlchemy API.


class PsgDBClient:
    def __init__(self, database_url, database_port, database_name, database_user, database_pass) -> None:
        """
        Initialize a new PostgresDB client object.

        :param database_url: PostgresDB host or IP.
        :param database_port: PostgresDB port.
        :param database_name: PostgresDB name.
        :param database_user: PostgresDB user.
        :param database_pass: PostgresDB password.
        """
        self.engine = create_engine(f'postgresql://{database_user}:{database_pass}@{
                                    database_url}:{database_port}/{database_name}')
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def init_tables(self):
        """
        Create the database schema and tables for raw data ingestion if they are not exist. 
        Tables will be dropped and recreated if they are already exist.

        :param database_url: PostgresDB host or IP.
        :param database_port: PostgresDB port.
        :param database_name: PostgresDB name.
        :param database_user: PostgresDB user.
        :param database_pass: PostgresDB password.
        """
        create_schema = CreateSchema('raw', if_not_exists=True)
        with self.engine.connect() as connection:
            connection.execute(create_schema)
            connection.commit()
        Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)
