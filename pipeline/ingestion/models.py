from typing import Optional
from sqlalchemy import Integer
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column

class Base(DeclarativeBase):
    pass

# A class represents SQLAlchemy data model for sales raw data.
class Sale(Base):
  __table_args__ = {'schema': 'raw'}
  __tablename__ = 'sales'

  id = mapped_column(Integer, primary_key=True)
  order_id : Mapped[Optional[int]]
  customer_id : Mapped[Optional[int]]
  product_id : Mapped[Optional[int]]
  quantity : Mapped[Optional[int]]
  price : Mapped[Optional[float]]
  order_date : Mapped[Optional[str]]
  customer_name : Mapped[Optional[str]]
  customer_username : Mapped[Optional[str]] 
  customer_email : Mapped[Optional[str]]
  customer_lat : Mapped[Optional[str]]
  customer_lng : Mapped[Optional[str]]
  weather_condition : Mapped[Optional[str]]
  weather_temp : Mapped[Optional[float]] 