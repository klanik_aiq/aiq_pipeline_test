import logging
import os
import pandas as pd
from datetime import datetime, timezone
from settings import *
from models import Sale
from api_client import ApiClient
from db_client import PsgDBClient

# Initialize the logger file handler to writting logs into a file
file_handler = logging.FileHandler('log.log')

# Initialize the logger object
logger = logging.getLogger()
logger.addHandler(file_handler)
logger.setLevel(logging.INFO)

# Initialize PostgresDB client
psgClient = PsgDBClient(database_url, database_port,
                        database_name, database_user, database_pass)

# Initialize customer API client
customer_api_client = ApiClient(customer_api_base_url)
# Initialize weather API client
weather_api_client = ApiClient(weather_api_base_url)


def convert_date_to_unix_utc(date_string):
    """
    Convrt a date string to unix timestamp in UTC timezone.

    :param date_string: The date string to convert.
    """
    # Parse the date string to a datetime object
    date_obj = datetime.strptime(date_string, "%Y-%m-%d")

    # Convert to UTC timezone
    date_obj = date_obj.astimezone(timezone.utc)

    # Get the Unix timestamp without milliseconds
    unix_time = int(date_obj.timestamp())

    return unix_time


def ingest_customers():
    """
    Request the customer's API and ingest the respons' customers dataset.
    """
    customer_df = pd.DataFrame()
    try:
        customer_response = customer_api_client.get('users', None, None)
        for customer_data in customer_response:
            customer = pd.DataFrame({
                'customer_id': [customer_data['id']],
                'customer_name': [customer_data['name']],
                'customer_username': [customer_data['username']],
                'customer_email': [customer_data['email']],
                'customer_lat': [customer_data['address']['geo']['lat']],
                'customer_lng': [customer_data['address']['geo']['lng']]
            })
            customer_df = pd.concat([customer_df, customer], ignore_index=True)

        return customer_df
    except Exception as e:
        logger.error(e.__str__())


def ingest_sales(path: str):
    """
    Ingest sales dataset from csv file.

    :param path: The csv file's path of sales dataset.
    """
    try:
        sales_df = pd.read_csv(path)
        return sales_df
    except Exception as e:
        logger.error(e.__str__())


def enrich_weather(mrgd_sale_df):
    """
    Get the weather info for each sale in the merged dataset based on the sale's customer coordinations.

    :param mrgd_sale_df: The sales dataset merged with customers dataset as a pandas dataframe.
    """
    try:
        weather_df = pd.DataFrame()
        for index, sale in mrgd_sale_df.iterrows():
            # Initialze weather's API request parameters for each sale record
            params = {
                'dt': convert_date_to_unix_utc(sale['order_date']),
                'lat': sale['customer_lat'], # lat of customer
                'lon': sale['customer_lng'], # lon of the customer
                'appid': weather_api_key
            }

            weather_response = weather_api_client.get('weather', None, params)
            
            mrgd_sale_df.loc[index, 'weather_condition'] = weather_response['weather'][0]['main']
            mrgd_sale_df.loc[index, 'weather_temp'] = weather_response['main']['temp']
            # Append weather condition and temperature for each sale to the weather dataframe
            # sale_weather = pd.DataFrame({
            #     'weather_condition': [weather_response['weather'][0]['main']],
            #     'weather_temp': [weather_response['main']['temp']]
            # })
            # weather_df = pd.concat(
            #     [weather_df, sale_weather], ignore_index=True)

        # return the weather dataframe   
        return mrgd_sale_df
    except Exception as e:
        logger.error(e.__str__())


def ingest_data():
    """
    Ingest the raw data for sales, customers and weather and merge them into one dataset, 
    then store them into PostgresDB sales table.
    """
    try:
        # Ingest sales dataset from csv file as a dataframe
        sales_ds_path = os.path.join(os.path.dirname(__file__), 'datasets/sales_data.csv')
        sales_df = ingest_sales(sales_ds_path)
        # Ingest customers dataset from customer's API as a dataframe
        customer_df = ingest_customers()
        # Merge sales and customers dataframes on the customer_id field.
        sale_customer_df = pd.merge(sales_df, customer_df, on='customer_id')
        # Sorting the merged dataframe based on oreder_id to enrich each sale with the corresponding weather info
        sorted_df = sale_customer_df.sort_values(by='order_id', ascending=True)
        # Get weather info for all sales
        final_df = enrich_weather(sorted_df)
        # Enrich the merged dataframe with weather info for each sale
        #final_df = pd.concat([sorted_df, weather_df], axis=1)
        # Store the final dataframe into PostgresDB sales table
        final_df.to_sql(Sale.__tablename__, psgClient.engine, schema=Sale.__table_args__[
                        'schema'], index=False, if_exists="append")
    except Exception as e:
        logger.error(e.__str__())


def main():
    # Initialize PostgresDB schema and tables
    psgClient.init_tables()
    # Ingest and merge sales, customers and weather raw data into PostgresDB sales table
    ingest_data()


if __name__ == '__main__':
    main()