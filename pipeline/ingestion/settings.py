import os

# Settings used by the ingestion application, defined as environment variables.
customer_api_base_url = os.getenv('CUSTOMER_API_BASE_URL', 'https://jsonplaceholder.typicode.com')
weather_api_base_url = os.getenv('WEATHER_API_BASE_URL', 'https://api.openweathermap.org/data/2.5')
weather_api_key = os.getenv('WEATHER_API_KEY', '65891f9f25374b5f1aa04bac9decf8c7')
database_url = os.getenv('DATABASE_URL', '127.0.0.1')
database_port = os.getenv('DATABASE_PORT', '5432')
database_user = os.getenv('DATABASE_USER', 'postgres')
database_pass = os.getenv('DATABASE_PASS', 'postgres')
database_name = os.getenv('DATABASE_NAME', 'postgres')