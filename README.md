
# 1 Data Architecture

We followed a layered approach in designing the data architecture. Here are the data layers.

## 1.1 Raw Data Layer

This layer represents the raw data layer, where we store the ingested raw sales, users, and weather data in a merged dataset/table as illustrated by the table below:

![Alt text](<images/postgres - raw - sales.png>)

## 1.2 Trusted Data Layer

This represents the cleaned and enriched data layer, where we store the merged sales data after the required transformations, enrichments, and cleaning, as illustrated by the table below:

![Alt text](<images/postgres - dev_trusted - sales.png>)  

- Data type transformations: the sales dataset (table) in the trusted layer has the "order_date" column as a date column type instead of a string in the raw dataset; "customer_lat" and "customer_lng" are float columns in the trusted sales dataset instead of strings in the raw dataset.  
- Data enrichments: The sales dataset in the trusted layer has a new column, "tot_price", which is the result of multiplying the price and the quantity for each order, and it's defined as a float column type.

## 1.3 Dimensional Data Layer

This layer represents the data warehouse layer, where data is organised into a dimensional structure as illustrated in the table's images below:

![Alt text](<images/postgres - dev_dim.png>)

- fct_sales table: sales fact table that contains all the measures needed for the required analysis and aggregations;
- dim_calendar table: date dimension of sales fact table with month, quarter, and year granularities;
- dim_customers table: customer dimension of sales fact table with relevant customer's information.

## 1.4 Aggregation Data Layer

This layer represents the aggregation and reporting data layer; it contains all the required aggregation tables, as illustrated by the image below:

![Alt text](<images/postgres - dev_agg.png>)

- avg_qt_product table: represents the average sale's quantity per product;
- avg_sales_weather table: represents the average sale's amount per weather condition;
- top_selling_customers table: represents the top 5 selling customers with the total sale's amount per customer;
- top_selling_products table: represents the top 5 selling products with the total amount for each;
- tot_sales_customer table: represents the total sale's amount per customer;
- sales_monthly table: represents the aggregation of the sale's amount per month;
- sales_quarterly table: represents the aggregation of the sale's amount per quarter;
- sales_yearly table: represents the aggregation of the sale's amount per year.

## 1.5 Audit Data Layer

This layer represents the auditing layer, where we store any audit information, like duplicated or invalid raw seal's records. We implemented an audit table for only duplication, as illustrated by the image below:

![Alt text](<images/postgres - dev_audit.png>)

This table has the same raw sales table structure plus "dup_column" which indicates the unique duplicated column in the record.

# 2 Data Processing Workflow

The data processing workflow consists of two main phases: data ingestion and data transformation.

## 2.1 Raw data ingestion (python, ingestion folder)

In this phase, the user's API data is ingested and merged with the sales dataset from the csv file in the "customer_id" column, and then the weather API is requested for each sale record based on the "customer_lat", "customer_lng" and "order_date" columns in the merged dataset. Lastly the ingested weather data is merged with sale customer dataset and the final dataset is stored into the raw data layer (postgres db, raw schema, raw.sales table) as illustrated in images below:

![Alt text](images/data_raw_sales_1.png)![Alt text](images/data_raw_sales_2.png)

## 2.2 Data Transformation (dbt SQL, transformations folder)

In this phase, the required data transformations are done using the following steps:

### 2.2.1 Auditing

Sales records with duplicate "order_id" are considered and inserted into tables in the auditing layer (postgres db, dev_audit schema, dev_audit.dup_sales table), as illustrated by the images below, where we can see records with duplicate "order_id":

![Alt text](images/data_audit_sales_1.png) ![Alt text](images/data_audit_sales_2.png)

### 2.2.2 Trusted Data Transformations

"order_date" is converted to date data type; "customer_lat" and "customer_lng" coordinates are converted to float data type. Lastly, "tot_price" is enriched as a new field to the sales dataset, which is the result of multiplying quantity and price for each sale. Lastly, the final result is stored in the trusted layer (postgres db, dev_trusted schema, dev_trusted.sales table), as illustrated by the image below:

![Alt text](images/data_trusted_sales_1.png)![Alt text](images/data_trusted_sales_2.png)

### 2.2.3 Dimensional Data Transformations

The sales dataset is transformed into a dimensional model (star schema) for running the required aggregation queries on top of the dim layer (Postgres DB, dev_dim schema), as illustrated by the images below:

![Alt text](images/data_dim_calendar.png)![Alt text](images/data_dim_customers.png) ![Alt text](images/data_fct_seals_1.png)

### 2.2.4 Aggregations

Required aggregations are defined and executed as dbt models, and the result of each aggregation is stored into a table in the aggregation layer (Postgres DB, dev_agg schema), as described below:

#### - Total Sales per Customer

![Alt text](images/data_agg_tot_seal_customer.png)![Alt text](images/vis_agg_tot_sale_customer.png)  

#### - Top 5 Selling Customers

![Alt text](images/data_agg_top_selling_customers.png)
![Alt text](images/vis_agg_top_selling_customers.png)

#### - AVG Order Quantity per Product

![Alt text](images/data_agg_avg_qt_prod.png)
![Alt text](images/vis_agg_avg_qt_prod.png)

#### - Top 5 Selling Products

![Alt text](images/data_agg_top_selling_products.png)
![Alt text](images/vis_agg_top_selling_products.png)

##### - Average Sales Amount per Weather Condition

![Alt text](images/data_agg_avg_sales_wthr.png)
![Alt text](images/vis_agg_avg_sales_wthr.png)

#### - Monthly Sales

![Alt text](images/data_agg_sales_month.png)
![Alt text](images/vis_agg_sales_month.png)

#### - Quarterly Sales

![Alt text](images/data_agg_sales_qrtr.png)
![Alt text](images/vis_agg_sales_qrtr.png)

#### - Yearly Sales

![Alt text](images/data_agg_sales_year.png)
![Alt text](images/vis_agg_sales_year.png)

# 3 Executing the code

The code is containerised using Docker and Docker-Compose, so you just need to run the following steps into the pipeline dir:

1- Run 'docker-compose up -d' command  
2- Wiat until the pipeline_aiq_etl service is done (container shutdown)  
3- Query postgres database tables as described previously. (host: localhost, port: 5432)
